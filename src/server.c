#include "server.h"

int main(const int args, const char *argv[])
{
	log_info("Start");

	fputs("-===[ Server ]===-\n", stdout);
	
	init();
	log_info("Stop");
	return EXIT_SUCCESS;
}

void init(void)
{
	log_info("init()");

	opt = 1;
	address_len = sizeof address;

	address.sin_family = AF_INET;
	address.sin_addr.s_addr = INADDR_ANY;
	address.sin_port = htons(PORT);

	if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0)
		log_error();

	if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR|SO_REUSEPORT,
												&opt, sizeof opt))
		log_error();

	if (bind(server_fd, (struct sockaddr *)&address, sizeof address) < 0)
		log_error();

	start();
	
	return;
}

void start(void)
{
	log_info("start()");

	if (listen(server_fd, 3) < 0)
		log_error();

	if ((client_fd = accept(server_fd, (struct sockaddr *)&address,
									(socklen_t *)&address_len)) < 0)
		log_error();

	request();
	getURL();
	response();
	getPage();
	stop();

	return;
}

void stop(void)
{
	log_info("stop()");

	shutdown(client_fd, SHUT_WR);
	close(client_fd);
	return;
}

void response(void)
{
	log_info("response()");

	send(client_fd, SERVER_RESPONSE, strlen(SERVER_RESPONSE), 0);

	//log_info("SERVER_RESPONSE:");
	//fprintf(stdout, "\n" YELLOW "%s" RESET "\n", SERVER_RESPONSE);
	log_info("SERVER_RESPONSE:" RESET "\n\n" YELLOW "%s" RESET, SERVER_RESPONSE);
	return;
}

void request(void)
{
	log_info("request()");

	char request_msg[BUFFER_SIZE];

	recv(client_fd, request_msg, BUFFER_SIZE, 0);

	//log_info("request_msg:");
	//fprintf(stdout, "\n" BLUE "%s" RESET, request_msg);
	log_info("request_msg:" RESET "\n\n" BLUE "%s" RESET, request_msg);
	return;
}

void getURL(void)
{
	log_info("getURL()");

	return;
}

void getPage(void)
{
	log_info("getPage()");
	system("ls site/pages/ > site/pages_list.txt");
	
	FILE *fp;
	char ch;
	char buffer[BUFFER_SIZE] = {};

	if((fp = fopen("site/pages/index.html", "r")) == NULL) {
	} else {
		for (int i = 0; ch != EOF; ++i) {
			if ((ch = getc(fp)) == EOF) break;
			buffer[i] = ch;
		}
		fclose(fp);
	}

	sendPage(buffer);

	return;
}

void sendPage(char *msg)
{
	log_info("sendPage()");

	send(client_fd, msg, strlen(msg), 0);

	//log_info("msg:");
	//fprintf(stdout, "\n" YELLOW "%s" RESET "\n", msg);
	log_info("msg:" RESET "\n\n" YELLOW "%s" RESET, msg);
	return;
}
