CC=gcc
FLAGS=-Wall

BIN=bin
SRC=src
OBJ=obj

DEBUG=-DDEBUG

all:
	@echo "make debug"
	@echo "make release"
	@echo "make clean"

debug: debug_server.o debug_server

release: server.o server

server.o: $(SRC)/server.c
	$(CC) $(FLAGS) -c $(SRC)/server.c -o $(OBJ)/server.o

server: $(OBJ)/server.o $(SRC)/server.h
	$(CC) $(FLAGS) $(OBJ)/server.o -o $(BIN)/server

debug_server.o: $(SRC)/server.c
	$(CC) $(FLAGS) $(DEBUG) -c $(SRC)/server.c -o $(OBJ)/server.o

debug_server: $(OBJ)/server.o $(SRC)/server.h
	$(CC) $(FLAGS) $(DEBUG) $(OBJ)/server.o -o $(BIN)/server

clean:
	@rm -rf $(BIN)/* &
	@rm -rf $(OBJ)/*.o &
	@rm -rf $(SRC)/*.h.gch &
